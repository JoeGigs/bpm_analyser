=begin
the console will accept a current bpm  and then ask for the original bpm for the next track.
the terminal will accept the second tracks bpm and spit out a value for the users to adjust the
pitch slider on the turntable
=end

$current_bpm = (print "enter current bpm: "; gets).to_i
$next_track_bpm = (print "enter next bpm: "; gets).to_i
$bpm_diff = 0

def compare_bpms  #should compare the two integers and output the difference in bpm
	if $current_bpm > $next_track_bpm
		# puts "the difference is #{$current_bpm - $next_track_bpm} bpm"
		$bpm_diff = $current_bpm - $next_track_bpm
		puts $bpm_diff
	elsif $next_track_bpm > $current_bpm
		# puts "the difference is #{$next_track_bpm - $current_bpm} bpm"
		$bpm_diff = $next_track_bpm - $current_bpm
		puts $bpm_diff
	end
end

def pitch_adjust #will assign a pitch value for each bpm higher or lower
	bpm_to_pitch = $bpm_diff * 0.8
	puts bpm_to_pitch
end

compare_bpms
pitch_adjust
